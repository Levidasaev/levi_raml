This API includes any service related to the management of accounts.  

An account is an arranged financial product between a bank and a user, where a user may deposit and withdraw money and, in some cases, be paid interest.

There are several types of accounts based on various parameters such as the origin of the balance (current account or credit account), currency (foreign currency account), functionality (savings), etc.